EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_SAMD:ATSAMD10C14A-SS U1
U 1 1 605B6DBA
P 5950 3750
F 0 "U1" H 5600 4400 50  0000 C CNN
F 1 "ATSAMD10C14A-SS" H 6350 3100 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5950 2700 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42242-SAM-D10_Datasheet.pdf" H 5950 3050 50  0001 C CNN
	1    5950 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 605B9302
P 6900 3100
F 0 "C1" H 6992 3146 50  0000 L CNN
F 1 "100nF" H 6992 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6900 3100 50  0001 C CNN
F 3 "~" H 6900 3100 50  0001 C CNN
	1    6900 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR05
U 1 1 605BC88C
P 6900 3000
F 0 "#PWR05" H 6900 2850 50  0001 C CNN
F 1 "+3.3V" H 6915 3173 50  0000 C CNN
F 2 "" H 6900 3000 50  0001 C CNN
F 3 "" H 6900 3000 50  0001 C CNN
	1    6900 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 605BD48A
P 6900 3200
F 0 "#PWR06" H 6900 2950 50  0001 C CNN
F 1 "GND" H 6905 3027 50  0000 C CNN
F 2 "" H 6900 3200 50  0001 C CNN
F 3 "" H 6900 3200 50  0001 C CNN
	1    6900 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR03
U 1 1 605BE5C5
P 5950 3050
F 0 "#PWR03" H 5950 2900 50  0001 C CNN
F 1 "+3.3V" H 5965 3223 50  0000 C CNN
F 2 "" H 5950 3050 50  0001 C CNN
F 3 "" H 5950 3050 50  0001 C CNN
	1    5950 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 605BEB09
P 5950 4450
F 0 "#PWR04" H 5950 4200 50  0001 C CNN
F 1 "GND" H 5955 4277 50  0000 C CNN
F 2 "" H 5950 4450 50  0001 C CNN
F 3 "" H 5950 4450 50  0001 C CNN
	1    5950 4450
	1    0    0    -1  
$EndComp
Text GLabel 5450 3450 0    50   Input ~ 0
~RST~
Text GLabel 5450 3650 0    50   Input ~ 0
SWCLK
Text GLabel 5450 3750 0    50   Input ~ 0
SWDIO
$Comp
L power:+3.3V #PWR01
U 1 1 605C6408
P 3800 3950
F 0 "#PWR01" H 3800 3800 50  0001 C CNN
F 1 "+3.3V" H 3815 4123 50  0000 C CNN
F 2 "" H 3800 3950 50  0001 C CNN
F 3 "" H 3800 3950 50  0001 C CNN
	1    3800 3950
	1    0    0    -1  
$EndComp
Text GLabel 4350 4100 3    50   Input ~ 0
~RST~
$Comp
L Switch:SW_Push SW1
U 1 1 605C75E9
P 4650 3300
F 0 "SW1" H 4650 3585 50  0000 C CNN
F 1 "Reset" H 4650 3494 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4650 3500 50  0001 C CNN
F 3 "~" H 4650 3500 50  0001 C CNN
	1    4650 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 605C7EB5
P 4850 3300
F 0 "#PWR02" H 4850 3050 50  0001 C CNN
F 1 "GND" H 4855 3127 50  0000 C CNN
F 2 "" H 4850 3300 50  0001 C CNN
F 3 "" H 4850 3300 50  0001 C CNN
	1    4850 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ALT D1
U 1 1 605D1251
P 7300 3900
F 0 "D1" H 7293 3645 50  0000 C CNN
F 1 "LED_ALT" H 7293 3736 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7300 3900 50  0001 C CNN
F 3 "~" H 7300 3900 50  0001 C CNN
	1    7300 3900
	-1   0    0    1   
$EndComp
Text GLabel 6450 3650 2    50   Input ~ 0
LED
Text GLabel 7150 3900 0    50   Input ~ 0
LED
$Comp
L Device:R R2
U 1 1 605D591D
P 7700 3900
F 0 "R2" V 7907 3900 50  0000 C CNN
F 1 "1K" V 7816 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7630 3900 50  0001 C CNN
F 3 "~" H 7700 3900 50  0001 C CNN
	1    7700 3900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 605D843D
P 7950 4000
F 0 "#PWR07" H 7950 3750 50  0001 C CNN
F 1 "GND" H 7955 3827 50  0000 C CNN
F 2 "" H 7950 4000 50  0001 C CNN
F 3 "" H 7950 4000 50  0001 C CNN
	1    7950 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3900 7550 3900
Wire Wire Line
	7850 3900 7950 3900
Wire Wire Line
	7950 3900 7950 4000
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J3
U 1 1 605DA4C3
P 7850 3150
F 0 "J3" H 7900 3467 50  0000 C CNN
F 1 "SWD_PGM" H 7900 3376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7850 3150 50  0001 C CNN
F 3 "~" H 7850 3150 50  0001 C CNN
	1    7850 3150
	1    0    0    -1  
$EndComp
Text GLabel 7650 3050 0    50   Input ~ 0
SWDIO
Text GLabel 7650 3150 0    50   Input ~ 0
SWCLK
Text GLabel 7600 3300 0    50   Input ~ 0
~RST~
Wire Wire Line
	7600 3300 7650 3300
Wire Wire Line
	7650 3300 7650 3250
$Comp
L power:+3.3V #PWR08
U 1 1 605DC86B
P 8250 2950
F 0 "#PWR08" H 8250 2800 50  0001 C CNN
F 1 "+3.3V" H 8265 3123 50  0000 C CNN
F 2 "" H 8250 2950 50  0001 C CNN
F 3 "" H 8250 2950 50  0001 C CNN
	1    8250 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 605DD44E
P 8250 3350
F 0 "#PWR09" H 8250 3100 50  0001 C CNN
F 1 "GND" H 8255 3177 50  0000 C CNN
F 2 "" H 8250 3350 50  0001 C CNN
F 3 "" H 8250 3350 50  0001 C CNN
	1    8250 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 3050 8250 3050
Wire Wire Line
	8250 3050 8250 2950
Wire Wire Line
	8150 3250 8250 3250
Wire Wire Line
	8250 3250 8250 3350
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 605E44A7
P 7350 4600
F 0 "J1" H 7300 4800 50  0000 L CNN
F 1 "GPIO_A" H 7200 4300 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7350 4600 50  0001 C CNN
F 3 "~" H 7350 4600 50  0001 C CNN
	1    7350 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 605E554A
P 7800 4600
F 0 "J2" H 7750 4800 50  0000 L CNN
F 1 "GPIO_B" H 7650 4300 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7800 4600 50  0001 C CNN
F 3 "~" H 7800 4600 50  0001 C CNN
	1    7800 4600
	-1   0    0    -1  
$EndComp
Text GLabel 5450 3950 0    50   Input ~ 0
GPIO_1
Text GLabel 5450 4050 0    50   Input ~ 0
GPIO_2
Text GLabel 6450 3750 2    50   Input ~ 0
GPIO_3
Text GLabel 6450 3850 2    50   Input ~ 0
GPIO_4
Text GLabel 6450 3550 2    50   Input ~ 0
GPIO_5
Text GLabel 6450 3450 2    50   Input ~ 0
GPIO_6
Text GLabel 6450 4050 2    50   Input ~ 0
GPIO_7
Text GLabel 6450 3950 2    50   Input ~ 0
GPIO_8
Text GLabel 7150 4500 0    50   Input ~ 0
GPIO_1
Text GLabel 7150 4600 0    50   Input ~ 0
GPIO_2
Text GLabel 7150 4700 0    50   Input ~ 0
GPIO_3
Text GLabel 7150 4800 0    50   Input ~ 0
GPIO_4
Text GLabel 8000 4500 2    50   Input ~ 0
GPIO_5
Text GLabel 8000 4600 2    50   Input ~ 0
GPIO_6
Text GLabel 8000 4700 2    50   Input ~ 0
GPIO_7
Text GLabel 8000 4800 2    50   Input ~ 0
GPIO_8
NoConn ~ 8150 3150
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 605F7980
P 3650 3150
F 0 "J4" H 3600 3250 50  0000 L CNN
F 1 "Battery" H 3550 2950 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 3650 3150 50  0001 C CNN
F 3 "~" H 3650 3150 50  0001 C CNN
	1    3650 3150
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:APE8865NR-33-HF-3 U2
U 1 1 605FAC3F
P 3200 4050
F 0 "U2" H 3200 4292 50  0000 C CNN
F 1 "APE8865NR-33-HF-3" H 3200 4201 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3200 4275 50  0001 C CIN
F 3 "http://www.tme.eu/fr/Document/ced3461ed31ea70a3c416fb648e0cde7/APE8865-3.pdf" H 3200 4000 50  0001 C CNN
	1    3200 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 605FB9BA
P 3200 4350
F 0 "#PWR0101" H 3200 4100 50  0001 C CNN
F 1 "GND" H 3205 4177 50  0000 C CNN
F 2 "" H 3200 4350 50  0001 C CNN
F 3 "" H 3200 4350 50  0001 C CNN
	1    3200 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 605FBCD5
P 3650 4250
F 0 "#PWR0102" H 3650 4000 50  0001 C CNN
F 1 "GND" H 3655 4077 50  0000 C CNN
F 2 "" H 3650 4250 50  0001 C CNN
F 3 "" H 3650 4250 50  0001 C CNN
	1    3650 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 605FC2C4
P 2750 4250
F 0 "#PWR0103" H 2750 4000 50  0001 C CNN
F 1 "GND" H 2755 4077 50  0000 C CNN
F 2 "" H 2750 4250 50  0001 C CNN
F 3 "" H 2750 4250 50  0001 C CNN
	1    2750 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 605FCDAB
P 3650 4150
F 0 "C3" H 3742 4196 50  0000 L CNN
F 1 "1uF" H 3742 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3650 4150 50  0001 C CNN
F 3 "~" H 3650 4150 50  0001 C CNN
	1    3650 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 605FD326
P 2750 4150
F 0 "C2" H 2500 4200 50  0000 L CNN
F 1 "1uF" H 2500 4100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2750 4150 50  0001 C CNN
F 3 "~" H 2750 4150 50  0001 C CNN
	1    2750 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4050 2750 4050
Wire Wire Line
	3500 4050 3650 4050
$Comp
L power:+BATT #PWR0104
U 1 1 605FEAC6
P 2600 3950
F 0 "#PWR0104" H 2600 3800 50  0001 C CNN
F 1 "+BATT" H 2615 4123 50  0000 C CNN
F 2 "" H 2600 3950 50  0001 C CNN
F 3 "" H 2600 3950 50  0001 C CNN
	1    2600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3950 2600 4050
Wire Wire Line
	2600 4050 2750 4050
Connection ~ 2750 4050
Wire Wire Line
	3650 4050 3800 4050
Wire Wire Line
	3800 4050 3800 3950
Connection ~ 3650 4050
$Comp
L power:GND #PWR0105
U 1 1 6060CF0C
P 3400 3300
F 0 "#PWR0105" H 3400 3050 50  0001 C CNN
F 1 "GND" H 3405 3127 50  0000 C CNN
F 2 "" H 3400 3300 50  0001 C CNN
F 3 "" H 3400 3300 50  0001 C CNN
	1    3400 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0106
U 1 1 6060D3FA
P 3400 3100
F 0 "#PWR0106" H 3400 2950 50  0001 C CNN
F 1 "+BATT" H 3415 3273 50  0000 C CNN
F 2 "" H 3400 3100 50  0001 C CNN
F 3 "" H 3400 3100 50  0001 C CNN
	1    3400 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3150 3400 3150
Wire Wire Line
	3400 3150 3400 3100
Wire Wire Line
	3450 3250 3400 3250
Wire Wire Line
	3400 3250 3400 3300
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 606130C4
P 3400 3150
F 0 "#FLG0101" H 3400 3225 50  0001 C CNN
F 1 "PWR_FLAG" V 3400 3277 50  0000 L CNN
F 2 "" H 3400 3150 50  0001 C CNN
F 3 "~" H 3400 3150 50  0001 C CNN
	1    3400 3150
	0    -1   -1   0   
$EndComp
Connection ~ 3400 3150
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 60614115
P 3400 3250
F 0 "#FLG0102" H 3400 3325 50  0001 C CNN
F 1 "PWR_FLAG" V 3400 3377 50  0000 L CNN
F 2 "" H 3400 3250 50  0001 C CNN
F 3 "~" H 3400 3250 50  0001 C CNN
	1    3400 3250
	0    -1   -1   0   
$EndComp
Connection ~ 3400 3250
Text GLabel 4450 3300 0    50   Input ~ 0
~RST~
$Comp
L power:+3.3V #PWR0107
U 1 1 60610946
P 4350 3800
F 0 "#PWR0107" H 4350 3650 50  0001 C CNN
F 1 "+3.3V" H 4365 3973 50  0000 C CNN
F 2 "" H 4350 3800 50  0001 C CNN
F 3 "" H 4350 3800 50  0001 C CNN
	1    4350 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 605C5EC6
P 4350 3950
F 0 "R1" H 4420 3996 50  0000 L CNN
F 1 "10K" H 4420 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 4280 3950 50  0001 C CNN
F 3 "~" H 4350 3950 50  0001 C CNN
	1    4350 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC

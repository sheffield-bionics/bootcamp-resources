* Session 1
** Intro
*** Attendance
    - https://forms.gle/UkrWtnCFTPAo3J4W8
*** AGM Interest Form
    - https://forms.gle/coMVZHYwjV7H3vWW6
*** Address Feedback
    - I'll try to find some ML practice material over Easter
** Product Pages
   - https://www.microchip.com/wwwproducts/en/ATSAMD10C14
** Shortcuts
   - https://shortcutworld.com/KiCAD/win/KiCAD_Shortcuts
   - https://defkey.com/kicad-shortcuts
** Context
   - SWD :: https://electronics.stackexchange.com/questions/53571/jtag-vs-swd-debugging
   - Schematics :: https://learn.sparkfun.com/tutorials/how-to-read-a-schematic/all
   - Decoupling :: https://www.analog.com/media/en/training-seminars/tutorials/MT-101.pdf
   - Nested Schematics :: https://en.wikibooks.org/wiki/Kicad/eeschema/Hierarchical_Sheets
   - ERC & Power Flags :: https://forum.kicad.info/t/errtype-3-pin-connected-to-some-others-pins-but-no-pin-to-drive-it/10946
   - Insert Key Tip :: https://www.reddit.com/r/KiCad/comments/9afr0h/kicad_tip_use_the_insert_key_to_repeat_the_last/
   - Datasheet :: http://ww1.microchip.com/downloads/en/devicedoc/atmel-42242-sam-d10_datasheet.pdf
   - APE8865NR-33-HF-3 Regulator :: https://www.tme.eu/Document/ced3461ed31ea70a3c416fb648e0cde7/APE8865-3.pdf
** Conclusion
*** Feedback
    - https://forms.gle/VPJpqemuRJ5H8zmGA
* Session 2
** Intro
*** Attendance
    - https://forms.gle/UkrWtnCFTPAo3J4W8
*** AGM Interest Form
    - https://forms.gle/coMVZHYwjV7H3vWW6
** Hawk Link
   - https://github.com/MalphasWats/hawk
** Pcbnew
   - Tools > Update from Schematic
   - Could to lines in the Edge.Cuts layer
** OpenSCAD
   - Cheatsheet :: http://openscad.org/cheatsheet/
*** Source
#+BEGIN_SRC
$fn=50;
difference() {
    minkowski() {
        square([14,30], center=true);
        circle(2);
    }
    translate([ 7, 15]) circle(d=2);
    translate([ 7,-15]) circle(d=2);
    translate([-7, 15]) circle(d=2);
    translate([-7,-15]) circle(d=2);
}
#+END_SRC
** Pcbnew
   1) Import Graphics DXF (to fixed position)
   2) Pitch of breadboard is 100 mil
   3) Align headers, then filter select outline to centre
   4) Place the rest of components
   5) X to lay trace
** Eeschema
   1) Rearrange GPIO (From Pinout)
      - Datasheet :: http://ww1.microchip.com/downloads/en/devicedoc/atmel-42242-sam-d10_datasheet.pdf
      - D1 :: 1
      - G1 :: 2
      - G2 :: 3
      - G3 :: 4
      - G4 :: 5
      - G5 :: 14
      - G6 :: 13
      - G7 :: 10
      - G8 :: 9
** Pcbnew
   1) Update from schematic
   2) Finish traces
   3) Add a fill zone
      - JLC Capabilities :: https://jlcpcb.com/capabilities/Capabilities
      - B to refill zones
   4) Silkscreen! Copper layers off, m and r to move and rotate
   5) O to place footprint
** Conclusion
*** Feedback
    - https://forms.gle/uDfZpBCBHHG49zoR8

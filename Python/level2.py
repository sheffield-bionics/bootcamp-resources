from random import randrange

def guessing_game():
    # Generate a random number from 1-100 (inclusive)
    secret = randrange(1,101)

    # Print a welcome message
    print("I'm thinking a of a number between 1 and 100... Can you guess it?")

    # Loop down from 6 to 0 guesses left
    for left in reversed(range(7)):
        # Read guess as an integer
        guess = int(input("Your guess: "))
        # Check if it was correct and return early if so
        if guess == secret:
            print(f"Well done! The secret number was {secret}!")
            return
        # If we haven't returned, we're still guessing. Print the hint and
        # number of guesses remaining
        print(f"Sorry, that guess was too {'low' if guess < secret else 'high'}!")
        print(f"You have {left} guesses left...")

    # Not returning after 7 tries means that you failed to guess the number
    print("You failed to guess the number in time! Don't be sorry, be better...")

def word_reverser(sentence):
    # Allocate an empty list to hold the reversed words
    result = []
    # Loop through every word of the sentence
    for word in sentence.split():
        # Reverse the word and add it to the list
        result.append(word[::-1])
    # Join the words with a space and return the result
    return ' '.join(result)

# This uses "fancy" features that won't be covered until a future Python session
# Don't worry if parts of it look confusing! You can search for "Python generator
# expressions" if you'd like to know more.
def word_reverser_fancy(sentence):
    return ' '.join([word[::-1] for word in sentence.split()])


def pig_latin(filename):
    # Define a list of vowels (as a string) and an empty list for words
    vowels = 'aeiou'
    translated_words = []
    # Open the file specified in reading mode
    with open(filename, 'r') as f:
        # Loop through the individual words
        for word in f.read().split():
            tr_word = ''

            # Check if the first letter is a vowel
            if word[0].lower() in vowels:
                # If it is, just add 'way'
                tr_word = word + 'way'
            else:
                # Otherwise, loop through the letters until we find a vowel
                first_vowel = 0
                while word[first_vowel] not in vowels:
                    first_vowel += 1
                # Split at the vowel, sending the start to the end, and adding 'ay'
                tr_word = word[first_vowel:] + word[:first_vowel] + 'ay'

            translated_words.append(tr_word)

    # Join the translated words and write them to a new file
    with open('translated_' + filename, 'w') as f:
        f.write(' '.join(translated_words))


# This is another fancy solution with generators and fixing of punctuation and
# capitalization which is entirely optional
def pig_latin_fancy(filename):
    vowels = 'aeiou'
    punctuation = '.,;:!?'
    translated_words = []
    with open(filename) as f:
        for word in f.read().split():
            tr_word = ''

            # Pig-latin translation
            if word[0].lower() in vowels:
                tr_word = word + 'way'
            else:
                first_vowel = next(i for i, c in enumerate(word) if c in vowels)
                tr_word = word[first_vowel:] + word[:first_vowel] + 'ay'

            # Fix capitalization
            if word.istitle():
                tr_word = tr_word.capitalize()

            # Fix punctuation
            try:
                first_punct = next(i for i, c in enumerate(tr_word) if c in punctuation)
                tr_word = tr_word[:first_punct] + tr_word[first_punct + 1:] + tr_word[first_punct]
            except StopIteration:
                pass

            translated_words.append(tr_word)

    with open('translated_' + filename, 'w') as f:
        f.write(' '.join(translated_words))

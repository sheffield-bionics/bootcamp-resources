# NOTE: This file doesn't necessarily contain the "best" Python solutions to the
# exercises. These solutions are limited to what was introduced in the first
# lesson / worksheet and could be greatly improved by the use of more "advanced"
# Python features – particularly `for` + `range()` loops.

# Factorial Calculator
fact = 1
n = int(input('Please enter an integer: '))
while n > 1:
    fact *= n
    n -= 1
print('The result is:', fact)

# FizzBuzz
lst = []
x = 1
while x <= 100:
    buf = ''
    if x % 3 == 0:
        buf += 'Fizz'
    if x % 5 == 0:
        buf += 'Buzz'
    lst.append(buf or x)
    x += 1
print(lst)

# Tripoint Artist
x = 1
n = int(input('Triangle height: '))
while x <= n:
    print('#' * x)
    x += 1

x = 0
n = int(input('Pyramid height: '))
while x < n:
    row = x * 2 + 1
    print(' ' * (n - row + x) + '#' * row)
    x += 1

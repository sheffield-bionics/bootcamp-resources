class Vehicle:
    '''Represents a vehicle that consumes fuel to travel some distance'''
    on_the_road = []

    def __init__(self, model, fuel_cap, speed, mpg=30):
        self.model = model
        self.fuel_cap = fuel_cap
        self.speed = speed
        self.mpg = mpg
        self.fuel = fuel_cap
        self.odometer = 0
        self.on_the_road.append(self)

    def drive(self, distance):
        if (fuel_used := distance / self.mpg) <= self.fuel:
            minutes = distance / self.speed * 60
            self.fuel -= fuel_used
            self.odometer += distance
            return f"The {self.model} drove {distance} miles in {minutes:.1f} minutes."
        return f"The {self.model} didn't have enough fuel for the journey!"

    def refuel(self):
        needed = self.fuel_cap - self.fuel
        self.fuel += needed
        return f"Refuelled the {self.model} with {needed:.1f} gallons."

    @classmethod
    def count_models(cls, model):
        count = 0
        for v in cls.on_the_road:
            if v.model == model:
                count += 1
        return count

class Car(Vehicle):
    '''A specialised vehicle that contains some number of seats'''
    def __init__(self, model, seats, fuel_cap=10, speed=60, mpg=30):
        super().__init__(model, fuel_cap, speed, mpg)
        self.seats = seats

    def drive(self, distance, people):
        if people <= self.seats:
            return super().drive(distance)
        return f"The {self.model} didn't have enough seats for the journey!"
